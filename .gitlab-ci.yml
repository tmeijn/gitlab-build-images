# we support merge request workflow only
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

default:
  image: docker:git
  services:
    - docker:19.03.0-dind
  before_script:
    - docker login -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    - source scripts/build-helpers.sh
    - apk add -U bash
  tags:
    - gitlab-org-docker

stages:
  - test
  - deploy
  - automation

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375

# Run test job always
.test:
  stage: test
  rules:
    - when: always

# Run build job only on a default branch when triggered manually
.deploy:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

.test_build: &test_build
  extends: .test
  script:
    # Hack to set an array in /bin/sh
    # http://unix.stackexchange.com/a/137571
    - set -- $CI_JOB_NAME
    - docker pull "$CI_REGISTRY_IMAGE:$1" || true
    - docker build --cache-from="$CI_REGISTRY_IMAGE:$1" -f "Dockerfile.$1" .

.build_and_deploy: &build_and_deploy
  extends: .deploy
  script:
    # Hack to set an array in /bin/sh
    # http://unix.stackexchange.com/a/137571
    - set -- $CI_JOB_NAME
    - docker pull "$CI_REGISTRY_IMAGE:$1" || true
    - docker build --cache-from="$CI_REGISTRY_IMAGE:$1" --label "ci_pipeline_url=$CI_PIPELINE_URL" --label "ci_job_url=$CI_JOB_URL" -t "$CI_REGISTRY_IMAGE:$1" -f "Dockerfile.$1" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_JOB_NAME"

.test_custom: &test_custom
  extends: .test
  script:
    # Hack to set an array in /bin/sh
    # http://unix.stackexchange.com/a/137571
    - set -- $CI_JOB_NAME
    - ./scripts/custom-docker-build "$1"
  rules:
    - when: always

.build_and_deploy_custom: &build_and_deploy_custom
  extends: .deploy
  script:
    # Hack to set an array in /bin/sh
    # http://unix.stackexchange.com/a/137571
    - set -- $CI_JOB_NAME
    - ./scripts/custom-docker-build "$1" --label "ci_pipeline_url=$CI_PIPELINE_URL" --label "ci_job_url=$CI_JOB_URL" -t "$CI_REGISTRY_IMAGE:$1"
    - docker push "$CI_REGISTRY_IMAGE:$1"

# Tests

# Used by GDK verify step: https://gitlab.com/gitlab-org/gitlab-development-kit
node-10 test: *test_custom

# Used by Gitaly: https://gitlab.com/gitlab-org/gitaly/blob/master/.gitlab-ci.yml
ruby-2.6-golang-1.14-git-2.28 test: *test_custom
ruby-2.6-golang-1.15-git-2.28 test: *test_custom
ruby-2.6-golang-1.15-git-2.28-pgbouncer-1.14 test: *test_custom
ruby-2.7-golang-1.14-git-2.28 test: *test_custom
ruby-2.7-golang-1.15-git-2.28 test: *test_custom
ruby-2.7-golang-1.15-git-2.28-pgbouncer-1.14 test: *test_custom
ruby-2.7-golang-1.14-git-2.29 test: *test_custom
ruby-2.7-golang-1.15-git-2.29 test: *test_custom
ruby-2.7-golang-1.15-git-2.29-pgbouncer-1.14 test: *test_custom

# Used by GitLab: https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab-ci.yml
ruby-2.7.2.patched-golang-1.14-git-2.28-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 test: *test_custom
ruby-2.7.2.patched-golang-1.14-git-2.28-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 test: *test_custom
ruby-2.7.2.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 test: *test_custom
ruby-2.7.2.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 test: *test_custom
ruby-3.0.0.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 test: *test_custom
ruby-3.0.0.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 test: *test_custom

# Used by GitLab's compile-production-assets and compile-test-assets jobs
ruby-2.7.2-git-2.28-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34 test: *test_custom
ruby-2.7.2-git-2.29-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34 test: *test_custom
ruby-3.0.0-git-2.29-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34 test: *test_custom

# Used by www-gitlab-com's rspec job: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab-ci.yml
ruby-2.6.5-git-2.28-chrome-74.0-node-12.18-yarn-1.22-docker-19.03.1 test: *test_custom
ruby-2.6.5-git-2.28-chrome-85-node-12.18-yarn-1.22-docker-19.03.1 test: *test_custom
ruby-2.6.5-git-2.29-chrome-74.0-node-12.18-yarn-1.22-docker-19.03.1 test: *test_custom
ruby-2.6.5-git-2.29-chrome-85-node-12.18-yarn-1.22-docker-19.03.1 test: *test_custom
ruby-2.7.2-git-2.29-chrome-85-node-12.18-yarn-1.22-docker-19.03.1 test: *test_custom

# Used by customers-gitlab-com rspec job: https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/.gitlab-ci.yml
ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-81.0-node-12.x-yarn-1.21-postgresql-9.5-graphicsmagick-1.3.34: *test_custom

danger test: *test_build
alpine-aws test: *test_build
alpine-helm test: *test_build
ruby-alpine-aws test: *test_build
gitlab-charts-build-base test: *test_build
gitlab-charts-build-base-helm-3 test: *test_build
gitlab-helm3-kubectl1.14 test: *test_build
gitlab-qa-ruby-2.7 test: *test_build
gitlab-qa-ruby-3.0 test: *test_build
gitlab-qa-alpine-ruby-2.7 test: *test_build
gitlab-qa-alpine-ruby-3.0 test: *test_build
gitlab-puppeteer test: *test_build
omnibus-gitlab-bionic test: *test_build
omnibus-gitlab-centos7 test: *test_build
omnibus-gitlab-centos8 test: *test_build
omnibus-gitlab-jessie test: *test_build
omnibus-gitlab-opensuse15.1 test: *test_build
omnibus-gitlab-opensuse15.2 test: *test_build
omnibus-gitlab-stretch test: *test_build
omnibus-gitlab-buster test: *test_build
omnibus-gitlab-xenial test: *test_build
omnibus-gitlab-focal test: *test_build
omnibus-gitlab-depscan test: *test_build
omnibus-gitlab-cve-search test: *test_build

release-tools test: *test_build
sitespeed-gitlab test: *test_build
ubi-release test: *test_build
www-gitlab-com-2.7 test: *test_build
build-git: *test_build
terraform test: *test_build
# Used by go projects for linting https://gitlab.com/gitlab-org/gitlab/blob/master/doc/development/go_guide/index.md#automatic-linting
golangci-lint-alpine test: *test_build
# Used by gitlab-operator: https://gitlab.com/gitlab-org/gl-openshift/gitlab-operator
gitlab-operator-build-base test: *test_build

# Builds

# Used by GDK verify step: https://gitlab.com/gitlab-org/gitlab-development-kit
node-10 push: *build_and_deploy_custom

# Used by Gitaly: https://gitlab.com/gitlab-org/gitaly/blob/master/.gitlab-ci.yml
ruby-2.6-golang-1.14-git-2.28 push: *build_and_deploy_custom
ruby-2.6-golang-1.15-git-2.28 push: *build_and_deploy_custom
ruby-2.6-golang-1.15-git-2.28-pgbouncer-1.14 push: *build_and_deploy_custom
ruby-2.7-golang-1.14-git-2.28 push: *build_and_deploy_custom
ruby-2.7-golang-1.15-git-2.28 push: *build_and_deploy_custom
ruby-2.7-golang-1.15-git-2.28-pgbouncer-1.14 push: *build_and_deploy_custom
ruby-2.7-golang-1.14-git-2.29 push: *build_and_deploy_custom
ruby-2.7-golang-1.15-git-2.29 push: *build_and_deploy_custom
ruby-2.7-golang-1.15-git-2.29-pgbouncer-1.14 push: *build_and_deploy_custom

# Used by GitLab CE/EE: https://gitlab.com/gitlab-org/gitlab-ee/blob/master/.gitlab-ci.yml
ruby-2.7.2.patched-golang-1.14-git-2.28-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-2.7.2.patched-golang-1.14-git-2.28-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-2.7.2.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-2.7.2.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-3.0.0.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-3.0.0.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom

# Used by GitLab's compile-production-assets and compile-test-assets jobs
ruby-2.7.2-git-2.28-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-2.7.2-git-2.29-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34 push: *build_and_deploy_custom
ruby-3.0.0-git-2.29-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34 push: *build_and_deploy_custom

# Used by www-gitlab-com's rspec job: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab-ci.yml
ruby-2.6.5-git-2.28-chrome-74.0-node-12.18-yarn-1.22-docker-19.03.1 push: *build_and_deploy_custom
ruby-2.6.5-git-2.28-chrome-85-node-12.18-yarn-1.22-docker-19.03.1 push: *build_and_deploy_custom
ruby-2.6.5-git-2.29-chrome-74.0-node-12.18-yarn-1.22-docker-19.03.1 push: *build_and_deploy_custom
ruby-2.6.5-git-2.29-chrome-85-node-12.18-yarn-1.22-docker-19.03.1 push: *build_and_deploy_custom
ruby-2.7.2-git-2.29-chrome-85-node-12.18-yarn-1.22-docker-19.03.1 push: *build_and_deploy_custom

# Used by customers-gitlab-com rspec job: https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/.gitlab-ci.yml
ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-81.0-node-12.x-yarn-1.21-postgresql-9.5-graphicsmagick-1.3.34 push: *build_and_deploy_custom

danger push: *build_and_deploy
alpine-aws push: *build_and_deploy
alpine-helm push: *build_and_deploy
ruby-alpine-aws push: *build_and_deploy
gitlab-charts-build-base push: *build_and_deploy
gitlab-charts-build-base-helm-3 push: *build_and_deploy
gitlab-helm3-kubectl1.14 push: *build_and_deploy
gitlab-qa-ruby-2.7 push: *build_and_deploy
gitlab-qa-ruby-3.0 push: *build_and_deploy
gitlab-qa-alpine-ruby-2.7 push: *build_and_deploy
gitlab-qa-alpine-ruby-3.0 push: *build_and_deploy
gitlab-puppeteer push: *build_and_deploy
omnibus-gitlab-bionic push: *build_and_deploy
omnibus-gitlab-centos7 push: *build_and_deploy
omnibus-gitlab-centos8 push: *build_and_deploy
omnibus-gitlab-jessie push: *build_and_deploy
omnibus-gitlab-opensuse15.1 push: *build_and_deploy
omnibus-gitlab-opensuse15.2 push: *build_and_deploy
omnibus-gitlab-stretch push: *build_and_deploy
omnibus-gitlab-buster push: *build_and_deploy
omnibus-gitlab-xenial push: *build_and_deploy
omnibus-gitlab-focal push: *build_and_deploy
omnibus-gitlab-depscan push: *build_and_deploy
omnibus-gitlab-cve-search push: *build_and_deploy
release-tools push: *build_and_deploy
sitespeed-gitlab push: *build_and_deploy
ubi-release push: *build_and_deploy
www-gitlab-com-2.7 push: *build_and_deploy
build-git push: *build_and_deploy
terraform push: *build_and_deploy
# Used by go projects for linting https://gitlab.com/gitlab-org/gitlab/blob/master/doc/development/go_guide/index.md#automatic-linting
golangci-lint-alpine push: *build_and_deploy
# Used by gitlab-operator: https://gitlab.com/gitlab-org/gl-openshift/gitlab-operator
gitlab-operator-build-base push: *build_and_deploy

cache-google-chrome:
  stage: automation
  variables:
    MOUNT_POINT: /builds/$CI_PROJECT_PATH/mnt
  script:
    - mkdir -p "$MOUNT_POINT"
    - cp scripts/cache-google-chrome $MOUNT_POINT
    - docker run -e "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID" -e "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" -v "$MOUNT_POINT:/mnt" ubuntu:16.04 /mnt/cache-google-chrome

# Old Google Chrome images are listed here for reference:

# These still exist in our docker repository, but they cannot be built anymore since
# Google yanks them from their PPA.  To build variations of these requires an image based on
# one of the images below.

# See https://gitlab.com/gitlab-org/gitlab-build-images/merge_requests/54 for an example

# ruby-2.3.3-golang-1.8-git-2.7-chrome-59.0-node-7.1-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.3.3-golang-1.8-git-2.7-chrome-60.0-node-7.1-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.3.3-golang-1.8-git-2.13-chrome-59.0-node-7.1-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.3.3-golang-1.8-git-2.13-chrome-60.0-node-7.1-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.3.3-golang-1.8-git-2.13-chrome-61.0-node-8.x-yarn-1.0-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.3.5-golang-1.8-git-2.13-chrome-61.0-node-8.x-yarn-1.0-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.3.5-golang-1.8-git-2.13-chrome-62.0-node-8.x-yarn-1.2-postgresql-9.6 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-73.0-node-12.x-yarn-1.21-postgresql-9.6-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-73.0-node-12.x-yarn-1.21-postgresql-10-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-73.0-node-12.x-yarn-1.21-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-81.0-node-12.x-yarn-1.21-postgresql-9.6-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-81.0-node-12.x-yarn-1.21-postgresql-10-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.26-lfs-2.9-chrome-81.0-node-12.x-yarn-1.21-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.27-lfs-2.9-chrome-83-node-12.x-yarn-1.21-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.27-lfs-2.9-chrome-83-node-12.x-yarn-1.21-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.28-lfs-2.9-chrome-84-node-12.x-yarn-1.21-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.6.6-golang-1.14-git-2.28-lfs-2.9-chrome-84-node-12.x-yarn-1.21-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.7.1-golang-1.14-git-2.28-lfs-2.9-chrome-84-node-12.x-yarn-1.21-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.7.1-golang-1.14-git-2.28-lfs-2.9-chrome-84-node-12.x-yarn-1.21-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.7.2-golang-1.14-git-2.28-lfs-2.9-chrome-85-node-12.18-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.7.2-golang-1.14-git-2.28-lfs-2.9-chrome-85-node-12.18-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.7.2-golang-1.14-git-2.29-lfs-2.9-chrome-85-node-12.18-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-2.7.2-golang-1.14-git-2.29-lfs-2.9-chrome-85-node-12.18-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-3.0.0-golang-1.14-git-2.29-lfs-2.9-chrome-85-node-12.18-yarn-1.22-postgresql-11-graphicsmagick-1.3.34 push: *build_and_deploy_custom
# ruby-3.0.0-golang-1.14-git-2.29-lfs-2.9-chrome-85-node-12.18-yarn-1.22-postgresql-12-graphicsmagick-1.3.34 push: *build_and_deploy_custom

# ruby-2.6.5-git-2.27-chrome-83-node-12.x-yarn-1.21-docker-19.03.1 push: *build_and_deploy_custom
# ruby-2.6.5-git-2.28-chrome-84-node-12.x-yarn-1.21-docker-19.03.1 push: *build_and_deploy_custom
